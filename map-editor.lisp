(in-package :wfx-base)

(defclass map-editor (ajax-widget)
  ((info :initarg :info
         :initform nil
         :accessor info)
   (slot-id :initarg :slot-id
            :initform nil
            :accessor slot-id)
   (map-type :initarg :map-type
             :initform nil
             :accessor map-type) 
   (parameters :initarg :parameters
               :initform nil
               :accessor parameters))
  (:metaclass widget-class))

(defclass map-value ()
  ((key :initarg :key
        :initform nil
        :accessor key
        :key t)
   (value :initarg :value
          :initform nil
          :accessor value
          :key t))
  (:metaclass wfx-class))

(defclass map-grid (generic-grid)
  ((object :initarg :object
           :initform nil
           :accessor object)
   (map-type :initarg :map-type
             :initform nil
             :accessor map-type) 
   (map-editor :initarg :map-editor
               :initform nil
               :accessor map-editor) 
   (modal :initarg :modal
          :initform nil
          :accessor modal))
  (:default-initargs :bottom-buttons nil
                     ;; :buttons (list )
                     ))

(defmethod render-header-buttons ((grid map-grid)))

(defmethod map-rows (map class (type (eql 'hash-table)))
  (loop for key being the hash-key of map
        using (hash-value value)
        collect (make-instance class :key key :value value)))

(defmethod map-rows (map class (type (eql 'alist)))
  (loop for (key . value) in map
        collect (make-instance class :key key :value value)))

(defmethod get-rows ((grid map-grid))
  (map-rows (object grid) (row-object-class grid) (map-type grid)))

(defmethod handle-action ((grid map-grid) (action (eql :select)))
  (let ((object (editing-row grid))
        (find-object (find-object grid)))
    (setf (value-info-value (info find-object))
          object)
    (defer-js (js-render find-object))
    (defer-js "$('#~a').modal(\"hide\");$('~a').change();"
      (name (modal grid))
      (slot-id find-object))
    (finish-editing grid)))

(defun make-map-class (key-type value-type)
  (eval `(defclass ,(gensym) (map-value)
           ((key :db-type ,key-type)
            (value :db-type ,value-type))
           (:metaclass wfx-class))))

(defmethod render ((widget map-editor) &key)
  (let ((object (value-info-value (info widget))))
   (cond ((post-parameter "find")
          (let* ((modal (make-widget 'modal :name (sub-name widget "modal")
                                            :save-button nil
                                            :size :full))
                 (grid (make-widget 'map-grid
                                    :name (sub-name widget "grid")
                                    :box nil
                                    :map-editor widget
                                    :row-object-class (apply #'make-map-class (parameters widget))
                                    :map-type (map-type widget)
                                    :modal modal)))
            (setf (header modal) "Map"
                  (content modal)
                  (render-to-string grid)
                  (object grid) object)
            (render modal))))
   (with-html
     (:div :class "row"
           (when object
             (htm
              (:label :class "control-label"
                      "Map")))
           (:button :class "btn"
                    :onclick
                    (js-render widget (js-pair "find" "t"))
                    "Edit")))))

(defun present-map (type parameters name info)
  (let ((editor (make-widget 'map-editor :name name
                                         :map-type type
                                         :slot-id (frmt "#~a #~a" *form-id* name)
                                         :parameters parameters)))
    (setf (value-info-no-accept info) t
          (info editor) info)
    (render editor)))

(defmethod present-list ((type (eql 'hash-table)) parameters name value info)
  (present-map type parameters name info))

(defmethod present-list ((type (eql 'alist)) parameters name value info)
  (present-map type parameters name info))

(defmethod accept-list ((type (eql 'hash-table)) parameters value info)
  (value-info-value info))

(defmethod accept-list ((type (eql 'alist)) parameters value info)
  (value-info-value info))


(defmethod present-list ((type (eql 'read-only)) parameters name value info)
  (present-map type parameters name info))
