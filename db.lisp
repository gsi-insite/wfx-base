(in-package :wfx-base)

(defun system-db ()
  (db *current-theme*))

(defclass date-doc ()
  ((start-date :initarg :start-date
               :initform nil
               :accessor start-date
               :db-type date
               :parser #'parse-date)
   (end-date :initarg :end-date
             :initform nil
             :accessor end-date
             :db-type date
             :parser #'parse-date
             :validate #'validate-end-date))
  (:metaclass wfx-mixin))

(defun validate-date-overlap ()
  (let ((start (info-slot-value 'start-date))
        (end (info-slot-value 'end-date)))
    (when (and (integerp start)
               (integerp end)
               (<= start end))
      (lambda (doc)
        (when (let ((existing-start (start-date doc))
                    (existing-end (end-date doc)))
                (and (integerp existing-start)
                     (integerp existing-end)
                     (<= start existing-end)
                     (>= end existing-start)))
          "The date range overlaps with an existing record")))))

(defun validate-end-date (end)
  (let ((start (info-slot-value 'start-date)))
    (when (and (integerp start)
               (< end start))
      "The start date must be earlier than the end date.")))

(defclass doc ()
  ((user :initarg :user
         :initform nil
         :accessor user)
   (log-action :initarg :log-action
               :initform nil
               :accessor log-action
               :documentation "Inserted, updated, deleted, rolledback."))
  (:metaclass wfx-mixin))
