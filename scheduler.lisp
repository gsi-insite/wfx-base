(in-package :wfx-scheduler)

(defvar *scheduler-queue*
  (lparallel.queue:make-queue))

(defvar *tasks* nil)
(defparameter *scheduler-frequnecy* 60)
(defvar *thread-lock* (sb-thread:make-mutex))
(defvar *thread* nil)

(defclass task ()
  ((run-time :initarg :run-time
             :initform nil
             :accessor run-time)
   (day-time :initarg :day-time
             :initform nil
             :accessor day-time) 
   (executed :initarg :executed
             :initform nil
             :accessor executed)
   (repeat :initarg :repeat
           :initform nil
           :accessor repeat)
   (theme :initarg :theme
          :initform wfx:*current-theme*
          :accessor theme)))

(defun add-task (task)
  (lparallel.queue:push-queue (cons :add task)
                              *scheduler-queue*))

(defun remove-task (task)
  (lparallel.queue:push-queue (cons :remove task)
                              *scheduler-queue*))

(defun reschedule-task (task)
  (lparallel.queue:push-queue (cons :reschedule task)
                              *scheduler-queue*))

(defgeneric run-task (task))

(defun schedule-tasks (&optional (now (get-universal-time)))
  (setf *tasks*
        (loop for task in *tasks*
              unless (and (executed task)
                          (not (repeat task)))
              do
              (cond ((executed task)
                     (setf (run-time task)
                           (+ (max (run-time task)
                                   now)
                              (repeat task)))
                     (setf (executed task) nil))
                    ((not (run-time task))
                     (setf (run-time task)
                           (+ now
                              (or (repeat task) 0)))))
              and
              collect task)))

(defun run-tasks (now)
  (loop for task in *tasks*
        when (and (not (executed task))
                  (< (- (run-time task) now) 2))
        do (handler-case
               (let ((wfx:*current-theme* (theme task)))
                 (run-task task))
             (error (c)
               (format t "Error running task ~s:~%~a" task c)))
           (setf (executed task) t))
  (schedule-tasks now))

(defun process-queue (timeout)
  (let ((value (lparallel.queue:try-pop-queue *scheduler-queue*
                                              :timeout timeout)))
    (typecase value
      ((cons (eql :add))
       (push (cdr value) *tasks*)
       t)
      ((cons (eql :remove))
       (alexandria:deletef *tasks* (cdr value))
       t)
      ((cons (eql :reschedule))
       (let ((task (cdr value)))
         (setf (executed task) nil
               (run-time task) nil))
       t))))

(defun scheduler ()
  (let* ((timeout *scheduler-frequnecy*)
         (next-wakeup (+ (get-universal-time)
                         timeout)))
    (schedule-tasks)
    (loop 
     (let* ((reschedule (process-queue timeout))
            (now (get-universal-time))
            (diff (- next-wakeup now)))
       (when reschedule
         (schedule-tasks now))
       (cond ((< diff 2)
              (run-tasks now)
              (setf timeout *scheduler-frequnecy*
                    next-wakeup (+ next-wakeup timeout)))
             (t
              (setf timeout diff)))))))

(defun start-scheduler ()
  (sb-thread:with-mutex (*thread-lock* :wait-p nil)
    (unless (and *thread*
                 (sb-thread:thread-alive-p *thread*))
      (setf *thread*
            (bt:make-thread #'scheduler :name "wfx-scheduler")))))
