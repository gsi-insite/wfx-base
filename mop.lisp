(in-package :wfx-base)

(defclass wfx-mixin (storable-mixin)
  ((collection :initarg :collection
               :initform nil
               :accessor collection)
   (report :initarg :report
           :initform nil
           :accessor report)
   (all-slots :initarg :all-slots
              :initform nil
              :accessor all-slots
              :documentation "Includes virtual slots")
   (label :initarg :label
          :initform nil
          :accessor label) 
   (url :initarg :url
        :initform nil
        :accessor url)
   (grid-filter-first-item :initarg :grid-filter-first-item
                           :initform t
                           :accessor grid-filter-first-item)))

(defclass wfx-class (wfx-mixin storable-class)
  ())

(defclass wfx-versioned-class (wfx-mixin storable-versioned-class)
  ())

(defclass wfx-customizable-class (customizable-class wfx-versioned-class)
  ())

;;;

(defclass wfx-slot (storable-slot)
  ((header :initarg :header
           :initform `(,*slot-dummy* nil)
           :accessor header)
   (order :initarg :order
          :initform `(,*slot-dummy* nil)
          :accessor order)
   (initial-value :initarg :initial-value
                  :initform `(,*slot-dummy* nil)
                  :accessor initial-value) 
   (label :initarg :label
          :initform `(,*slot-dummy* nil)
          :accessor label)
   (required :initarg :required
             :initform `(,*slot-dummy* nil)
             :accessor required)
   (virtual :initarg :virtual
            :initform `(,*slot-dummy* nil)
            :accessor virtual)
   (validate :initarg :validate
             :initform `(,*slot-dummy* nil)
             :accessor validate)
   (duplicate-check :initarg :duplicate-check
                    :initform `(,*slot-dummy* nil)
                    :accessor duplicate-check)
   ;; HTML code to be displayed when editing
   (description :initarg :description
                :initform `(,*slot-dummy* nil)
                :accessor description)
   (csv-header :initform nil
               :accessor csv-header)
   (csv-row :initform nil
            :accessor csv-row)
   (should-escape :initarg :escape
                  :initform `(,*slot-dummy* nil)
                  :accessor should-escape)
   (read-only :initarg :read-only
              :initform nil
              :accessor read-only)
   (initial-sort :initarg :initial-sort
                 :initform `(,*slot-dummy* nil)
                 :accessor initial-sort)
   (sort-key :initarg :sort-key
             :initform `(,*slot-dummy* nil)
             :accessor sort-key)
   (plist :initarg :plist
          :initform nil
          :accessor plist)))

(defclass wfx-versioned-slot (wfx-slot storable-versioned-slot)
  ())

(defclass wfx-direct-slot-definition
    (wfx-slot standard-direct-slot-definition)
  ())

(defclass wfx-effective-slot-definition
    (wfx-slot standard-effective-slot-definition)
  ())

(defclass wfx-versioned-direct-slot-definition
    (wfx-versioned-slot standard-direct-slot-definition)
  ())

(defclass wfx-versioned-effective-slot-definition
    (wfx-versioned-slot standard-effective-slot-definition)
  ())

(defclass wfx-customizable-effective-slot-definition
    (customizable-effective-slot-definition wfx-versioned-effective-slot-definition)
  ())

(defmethod direct-slot-definition-class ((class wfx-mixin)
                                         &rest initargs)
  (declare (ignore initargs))
  (find-class 'wfx-direct-slot-definition))

(defmethod effective-slot-definition-class ((class wfx-mixin) &key)
  (find-class 'wfx-effective-slot-definition))

(defmethod direct-slot-definition-class ((class wfx-versioned-class) &key)
  (find-class 'wfx-versioned-direct-slot-definition))

(defmethod effective-slot-definition-class ((class wfx-versioned-class)
                                            &key)
  (find-class 'wfx-versioned-effective-slot-definition))

(defmethod effective-slot-definition-class ((class wfx-customizable-class)
                                            &key name)
  (cond ((or
          (find-slot-class name (list class))
          (find-slot-class name (member-if (lambda (x)
                                             (member (class-name x)
                                                     '(wfx-customized-object doc entity-doc)))
                                           (class-precedence-list class))))
         (find-class 'wfx-versioned-effective-slot-definition))
        (t
         (find-class 'wfx-customizable-effective-slot-definition))))

;;;

(defun ensure-collection (name)
  (or (get-collection (system-db) name)
      (add-collection (system-db) name)))

(defun process-collection (class collection-spec)
  (cond (collection-spec
         (check-type (car collection-spec) string)
         (let* ((collection (ensure-collection (car collection-spec)))
                (name (class-name class))
                (accessor-name (string name))
                (last-char (char accessor-name (1- (length accessor-name))))
                (function-name (cond ((cadr collection-spec))
                                     ((char-equal last-char #\s)
                                      name)
                                     ((alexandria:ends-with-subseq "data" accessor-name
                                                                   :test #'char-equal)
                                      name)
                                     ((char-equal last-char #\y)
                                      (alexandria:symbolicate (subseq accessor-name 0
                                                                      (1- (length accessor-name)))
                                                              'ies))
                                     (t
                                      (alexandria:symbolicate name 's)))))
           (setf (collection class) collection)
           (proclaim `(ftype function ,function-name))
           (setf (fdefinition function-name) (lambda () (docs collection)))))
        (t
         (setf (collection class)
               (loop for class in (class-direct-superclasses class)
                     thereis (and (typep class 'wfx-mixin)
                                  (collection class)))))))

(defun initialize-wfx-class (class &key collection label url grid-filter-first-item &allow-other-keys)
  (process-collection class collection)
  (setf (label class) (or (car label)
                          (make-name (class-name class))))
  (when url
    (setf (url class) (car url)))
  (when grid-filter-first-item
    (setf (grid-filter-first-item class) (car grid-filter-first-item)))
  (cond ((report class)
         (pushnew class (report-classes *current-theme*)))
        (*current-theme*
         (setf (report-classes *current-theme*)
               (delete class (report-classes *current-theme*)))))
  class)

(defmethod initialize-instance :around ((class wfx-class)
                                        &rest args
                                        &key (default-superclass 'wfx-object))
  (let ((instance (apply #'initialize-storable-class #'call-next-method
                         class default-superclass args)))
    (apply #'initialize-wfx-class instance args)))

(defmethod reinitialize-instance :around ((class wfx-class)
                                          &rest args
                                          &key (default-superclass 'wfx-object))
  (let ((instance (apply #'initialize-storable-class #'call-next-method
                         class default-superclass args)))
    (apply #'initialize-wfx-class instance args)))


(defmethod initialize-instance :around ((class wfx-versioned-class)
                                        &rest args
                                        &key (default-superclass 'wfx-versioned-object))
  (let ((instance (apply #'call-next-method class :default-superclass default-superclass
                         args)))
    (apply #'initialize-wfx-class instance args)))

(defmethod reinitialize-instance :around ((class wfx-versioned-class)
                                          &rest args
                                          &key (default-superclass 'wfx-versioned-object))
  (let ((instance (apply #'call-next-method  class :default-superclass default-superclass
                         args)))
    (apply #'initialize-wfx-class instance args)))

(defmethod initialize-instance :around ((class wfx-customizable-class)
                                        &rest args)
  (apply #'call-next-method class :default-superclass 'wfx-customized-object
         args))

(defmethod reinitialize-instance :around ((class wfx-customizable-class)
                                          &rest args)
  (apply #'call-next-method  class :default-superclass 'wfx-customized-object
         args))

;;;

(defun finalized-prototype (class)
  (unless (class-finalized-p class)
    (finalize-inheritance class))
  (class-prototype class))

(defmethod compute-effective-slot-definition
    ((class wfx-mixin) slot-name direct-definitions)
  (call-next-method)
  (let ((effective-definition (call-next-method))
        (direct-definition (car direct-definitions))
        (slots '(header order required
                 #'virtual #'validate #'duplicate-check #'sort-key
                 label description should-escape read-only
                 (eval initial-value)
                 initial-sort)))
    (when (typep direct-definition 'wfx-slot)
      (loop for slot in slots
            do
            (compute-slot-option effective-definition
                                 slot
                                 direct-definitions))
      (setf (plist effective-definition)
            (append
             (plist effective-definition)
             (loop for slot in direct-definitions
                   when (typep slot 'wfx-slot)
                   append (plist slot)))))
    (loop for slot-description in slots
          for slot = (if (consp slot-description)
                         (second slot-description)
                         slot-description)
          for value = (slot-val effective-definition slot)
          when (and (consp value)
                    (eq (car value) *slot-dummy*))
          do (setf (slot-value effective-definition slot) (cadr value)))
    (when (db-type effective-definition)
      (unless (label effective-definition)
        (setf (label effective-definition) (make-name slot-name)))
      (unless (slot-printer effective-definition)
        (setf (slot-printer effective-definition)
              (make-slot-printer (db-type effective-definition))))
      (unless (header effective-definition)
        (setf (header effective-definition) (key effective-definition)))
      (when (key effective-definition)
        (setf (required effective-definition) t))
      (unless (stringp (description effective-definition))
        (setf (description effective-definition)
              (eval `(with-html-string ,(description effective-definition)))))
      (let* ((db-type (db-type effective-definition))
             (export-type (if (typep db-type '(cons (eql :object)))
                              (finalized-prototype (find-class (second db-type)))
                              db-type)))
        (setf (csv-header effective-definition)
              (csv-exporter-header export-type)
              (csv-row effective-definition)
              (csv-exporter-row export-type))))
    effective-definition))

(defmethod compute-slots ((class wfx-mixin))
  (let* ((slots (call-next-method))
         (sorted (stable-sort
                  (loop for slot in slots
                        collect (cons (cond ((order slot))
                                            ((numberp (header slot))
                                             (header slot))
                                            (t
                                             0))
                                      slot))
                  #'< :key #'car)))
    (setf (all-slots class)
          (map-into sorted #'cdr sorted))
    (remove-if #'virtual slots)))

;;;

(defclass wfx-object (storable-object)
  ()
  (:metaclass wfx-class))

(defclass wfx-versioned-object (storable-versioned-object wfx-object)
  ((stamp-date :db-type date
               :order #.most-positive-fixnum
               :required t
               :initial-value (get-universal-time)
               :validate check-backdate))
  (:metaclass wfx-versioned-class))

(defclass wfx-customized-object (customized-object wfx-versioned-object)
  ()
  (:metaclass wfx-customizable-class))

(defun check-backdate (date)
  (let* ((object (object *editor*))
         (value (slot-value-using-class (class-of object) object *current-slot*)))
    (when (and (id object)
               value (and (not (date= value date))
                          (> value date)))
      (format nil "can't be earlier than the current date ~a"
              (format-date value)))))

(defmethod doc-collection ((doc wfx-object))
  (collection (class-of doc)))

;;;

(defun find-in-collection (test class)
  (loop for doc across (docs (collection (find-class class)))
        when (funcall test doc)
        return doc))

(defun describe-wfx-object (object &optional (stream *standard-output*))
  (declare (wfx-object object))
  (let ((class (class-of object)))
    (loop for slot in (all-slots class)
          when (header slot)
          do (format stream "~a: ~a~%" (label slot)
                     (funcall (slot-printer slot)
                              (slot-value-using-class class object slot))))))
