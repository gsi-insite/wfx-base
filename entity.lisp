(in-package :wfx-base)

(defclass entity-type (doc)
  ((entity-type-name :initarg :entity-type-name
                     :initform nil
                     :accessor entity-type-name
                     :key t)
   (description :initarg :description
                :initform nil
                :accessor description))
  (:collection "entity-types")
  (:metaclass wfx-versioned-class))

(defclass xid-doc (doc)
  ((xid :initarg :xid
        :initform nil
        :accessor xid))
  (:metaclass storable-mixin))

(defclass entity (xid-doc)
  ((entity-type :initarg :entity-type
                :initform nil
                :accessor entity-type
                :db-type (:object entity-type))
   (entity-name :initarg :entity-name 
                :initform nil
                :accessor entity-name
                :key t
                :db-type string)
   (ldap-connection :initarg :ldap-connection
                    :initform nil
                    :accessor ldap-connection))
  (:collection "entities")
  (:metaclass wfx-versioned-class))

(defmethod print-object ((object entity) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (when (slot-boundp object 'entity-name)
      (princ (entity-name object) stream))))

(defclass entity-doc (doc)
  ((entity :initarg :entity
           :initform nil
           :accessor entity
           :db-type entity
           :key t
           :printer print-entity-name))
  (:metaclass wfx-mixin))

(defun entity-types-collection ()
  (get-collection (system-db) "entity-types"))

(defun get-entity-type (entity-type-name)
  (get-doc (entity-types-collection) entity-type-name :element 'entity-type-name))

(defun get-entity-type-by-id (entity-type-id)
  (get-doc (entity-types-collection) entity-type-id :element 'id))

(defun make-entity-type (entity-type-name description)
  (make-instance 'entity-type
                 :entity-type-name entity-type-name
                 :description description
                 :top-level t))

(defun entities-collection ()
  (get-collection (system-db) "entities"))


(defun entity-list ()
  (loop for entity across (entities)
        when (member (xid entity) (context))
        collect (list (xid entity)
                      (entity-name entity))))

(defun print-entity-name (doc)
  (typecase doc
    (entity
     (entity-name doc))
    (entity-doc
     (and (entity doc)
          (entity-name (entity doc))))))


(defun make-entity (entity-type entity-name)
  (make-instance 'entity
                 :entity-type (if (typep entity-type 'entity-type)
                                  entity-type
                                  (get-entity-type entity-type)) 
                 :entity-name entity-name
                 :top-level t))

(defun get-entity (entity-name)
  (get-doc (entities-collection) entity-name
           :test #'equalp
           :element 'entity-name))

(defun get-entity-by-id (id)
  (get-doc (entities-collection) id :element 'xid))

(defun match-entity (object)
  (and (typep object 'entity-doc)
       (assoc (entity object) (request-page-permissions *current-page*))
       t))

(defun check-entity-access (entity &optional (user (current-user)))
  (or (super-user-p user)
      (loop for (root entities) on (accessible-entities user) by #'cddr
            thereis (and (member (xid entity) entities) t))))

(defmethod object-description ((object entity))
  (entity-name object))

(defgeneric match-context-entities (doc))

(defmethod match-context-entities ((doc t))
  t)

(defmethod match-context-entities ((doc entity-doc))
  (match-entity doc))

(defun print-context ()
  (with-output-to-string (str)
    (loop for space = "" then ", "
          for id in (context)
          for entity = (get-entity-by-id id)
          when entity
          do
          (princ space str)
          (princ (entity-name entity) str))))
