(in-package :wfx-base)

(defun copy-slots (from to)
  (let ((class-from (class-of from))
        (class-to (class-of to)))
    (loop for slotd in (class-slots class-from)
          when (slot-boundp-using-class class-from from slotd)
          do (setf (slot-value-using-class class-to to slotd)
                   (slot-value-using-class class-from from slotd)))
    to))

(defun copy-into (from to)
  (assert (eq (class-of from) (class-of to)))
  (copy-slots from to))

(defun copy-object (object &rest initargs)
  (let* ((class (class-of object))
         (new (allocate-instance class)))
    (loop for slotd in (c2mop:class-slots class)
          when (c2mop:slot-boundp-using-class class object slotd)
          do (setf (c2mop:slot-value-using-class class new slotd)
                   (c2mop:slot-value-using-class class object slotd)))
    (when initargs
      (apply #'reinitialize-instance new initargs))
    new))
