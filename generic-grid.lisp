(in-package :wfx-base)

(defclass generic-grid (grid)
  ((read-only-slots :initarg :read-only-slots
                    :initform nil
                    :accessor read-only-slots)
   (object-editor :initarg :object-editor
                  :accessor object-editor)
   (slot-filters :initarg :slot-filters
                 :initform nil
                 :accessor slot-filters))
  (:metaclass widget-class)
  (:default-initargs :top-level t))

(defclass generic-sub-grid (generic-grid)
  ((info :initarg :info
         :initform nil
         :accessor info)
   (slot :initarg :slot
         :initform nil
         :accessor slot)
   (initargs :initarg :initargs
             :initform nil
             :accessor initargs))
  (:metaclass widget-class)
  (:default-initargs :top-level nil
                     :modal-edit nil))

(defun make-generic-grid-columns (class)
  (c2cl:ensure-finalized class)
  (let (initial-sort)
   (values
    (loop with i = -1
          for slot in (all-slots class)
          for slot-name = (slot-definition-name slot)
          when (header slot)
          collect (make-instance 'grid-column
                                 :name (or (virtual slot)
                                           slot-name)
                                 :header (label slot)
                                 :printer (slot-printer slot)
                                 :special-printer (special-slot-printer slot)
                                 :escape (should-escape slot)
                                 :sort-key (sort-key slot))
          and do (incf i)
                 (when (initial-sort slot)
                   (setf initial-sort (list i (initial-sort slot)))))
    initial-sort)))

(defgeneric class-grid-buttons (class))

(defmethod initialize-instance :after ((grid generic-grid)
                                       &key row-object-class
                                            parent-grid
                                            (buttons nil buttons-p)
                                            slot-filters)
  (declare (ignore buttons))
  (multiple-value-bind (columns initial-sort)
      (make-generic-grid-columns row-object-class)
    (setf (columns grid)
          columns
          (object-editor grid)
          (make-widget 'object-editor :name (sub-name grid "object-editor")
                                      :parent (and parent-grid
                                                   (object-editor parent-grid))
                                      :grid grid)
          (slot-filters grid)
          (loop for (param . slot) in slot-filters
                for value = (parameter param)
                when value
                collect (cons slot value)))
    (when initial-sort
      (setf (initial-sort-column grid) initial-sort)))
  (setf (filter-first-item grid)
        (grid-filter-first-item row-object-class))
  (unless buttons-p
    (setf (buttons grid)
          (class-grid-buttons (class-prototype row-object-class)))))

(defgeneric class-grid-filters (class))
(defmethod class-grid-filters ((class t)))

(defgeneric filter-class-data (class objects filter))

(defmethod list-grid-filters ((grid generic-grid))
  (let ((filters (class-grid-filters (class-prototype (row-object-class grid)))))
    (if (typep (row-object-class grid) 'wfx-versioned-class)
        (cons 'with-audit-data filters)
        filters)))

(defmethod get-rows ((grid generic-grid))
  (let* ((filter (grid-filter grid))
         (class (row-object-class grid))
         (docs (docs (collection class)))
         (slot-filters (slot-filters grid)))
    (flet ((match-filter (doc)
             (loop for (slot . value) in slot-filters
                   always (equalp (slot-value doc slot) value))))
      (setf (rows grid)
            (typecase filter
              ((eql with-audit-data)
               (loop for doc across docs
                     when (and (typep doc class)
                               (match-context-entities doc)
                               (match-filter doc))
                     collect doc
                     and
                     append (old-versions doc)))
              ((or null grid-filter)
               (loop for doc across docs
                     when (and (typep doc class)
                               (match-context-entities doc)
                               (match-filter doc))
                     collect doc))
              (t
               (filter-class-data (class-prototype class)
                                  (if slot-filters
                                      (remove-if-not #'match-filter docs)
                                      docs)
                                  filter)))))))

(defmethod handle-action :after ((grid generic-grid) (action (eql :new)))
  (let* ((object (editing-row grid))
         (class (class-of object)))
    (loop for (slot . value) in (slot-filters grid)
          do (setf (slot-value object slot) value))
    (loop for slot in (class-slots class)
          for init = (and (initial-value slot)
                          (funcall (initial-value slot)))
          when init
          do (setf (slot-value-using-class class object slot) init))))

(defmethod get-rows ((grid generic-sub-grid))
  (let ((filter (grid-filter grid))
        (docs (value-info-value (info grid))))
    (setf (rows grid)
          (case filter
            (with-audit-data
                (loop for doc being the element of docs
                      collect doc
                      append (old-versions doc)))
            (t
             docs)))))

(defmethod handle-action ((grid generic-sub-grid) (action (eql :new)))
  (let ((class (row-object-class grid)))
    (setf (editing-row grid)
          (apply #'make-instance class (initargs grid)))))

(defmethod render-row-editor :around ((grid generic-sub-grid) row)
  (let ((*parent-slot-value-info* (slot-value-info (object-editor (parent-grid grid)))))
    (call-next-method)))

(defmethod render-row-editor ((grid generic-grid) row)
  (unless (typep grid 'generic-sub-grid)
    (defer-js "$(window).on('beforeunload', function(){ return 'Unsaved changes'; });"))
  (setf (object (object-editor grid)) row)
  (render (object-editor grid)
          :modal-edit (modal-edit grid)
          :read-only-slots (read-only-slots grid)
          :target grid))

(defun check-already-exists (object iterator)
  (unless (id object)
    (let* ((class (class-of object))
           (accessors (key-accessors class))
           (comparers (loop for (slot-name . accessor) in accessors
                            collect (cons accessor
                                          (value-info-value
                                           (gethash slot-name *slot-value-info*)))))
           (duplicate-checks
             (loop for slot in (class-slots class)
                   for check = (and (typep slot 'wfx-slot)
                                    (duplicate-check slot)
                                    (funcall (duplicate-check slot)))
                   when check
                   collect (cons check (slot-definition-name slot))))
           (check (lambda (doc)
                    (and (typep doc class)
                         (loop for (accessor . value) in comparers
                               always (equalp (funcall accessor doc) value))
                         (or (not duplicate-checks)
                             (loop for (check . slot) in duplicate-checks
                                   for result = (funcall check doc)
                                   when result
                                   collect (cons result slot))))))
           (exists
             (and comparers
                  (funcall iterator
                           (if (typep object 'customized-object)
                               (let ((parent (value-info-value
                                              (gethash 'parent *slot-value-info*))))
                                 (lambda (doc)
                                   (or (funcall check doc)
                                       (and (typep doc class)
                                            (eq (parent doc) parent)
                                            :parent))))
                               check)))))
      (cond ((consp exists)
             (loop for (error . slot-name) in exists
                   for info = (gethash slot-name *slot-value-info*)
                   do (setf (value-info-invalid info) error))
             t)
            (exists
             (loop for (slot-name) in (if (eq exists :parent)
                                          '((parent))
                                          accessors)
                   for info = (gethash slot-name *slot-value-info*)
                   do (setf (value-info-invalid info)
                            (if (eq exists :parent)
                                "Document with this parent already exists."
                                "Document with this key already exists.")))
             t)))))

(defun already-exists-in-collection (object)
  (check-already-exists
   object
   (lambda (test)
     (loop for doc across (docs (collection (class-of object)))
           thereis (funcall test doc)))))

(defun already-exists-in-parent (object parent-info)
  (check-already-exists
   object
   (lambda (test)
     (loop for doc in (value-info-value parent-info)
           thereis (and (not (eq object doc))
                        (funcall test doc))))))

(defgeneric generic-grid-persist (doc))

(defmethod generic-grid-persist :before ((doc doc))
  (setf (log-action doc) :edited))

(defmethod generic-grid-persist ((doc wfx-versioned-object))
  (persist doc :set-time nil))

(defmethod generic-grid-persist ((doc wfx-object))
  (persist doc))

(defmethod handle-action ((grid generic-grid) (action (eql :save)))
  (defer-js "$(window).off('beforeunload');")
  (let* ((row (editing-row grid))
         (*editor* (object-editor grid))
         (*slot-value-info* (slot-value-info *editor*))
         (success (accept-doc row)))
    (when (already-exists-in-collection row)
      (setf success nil))
    (when success
      (commit-values row *slot-value-info*)
      (clrhash *slot-value-info*)
      (generic-grid-persist row)
      (finish-editing grid))))

(defmethod handle-action ((grid generic-sub-grid) (action (eql :save)))
  (let* ((row (editing-row grid))
         (*editor* (object-editor grid))
         (*slot-value-info* (slot-value-info *editor*))
         (*parent-slot-value-info* (slot-value-info (parent *editor*)))
         (success (accept-doc row))
         (parent-info (info grid)))
    (when (already-exists-in-parent row parent-info)
      (setf success nil))
    (when success
      (commit-values row *slot-value-info*)
      (clrhash *slot-value-info*)
      (when (getf (initargs grid) :top-level)
        (generic-grid-persist row))
      (pushnew row (value-info-value parent-info))
      (finish-editing grid))))

(defmethod handle-action ((grid generic-sub-grid) (action (eql :delete)))
  (let ((row (editing-row grid))
        (parent-info (info grid)))
    (when (getf (initargs grid) :top-level)
      (remove-doc row (email (current-user))))
    (alexandria:removef (value-info-value parent-info) row)))

(defmethod handle-action :before ((grid generic-grid) (action (eql :edit)))
  (clrhash (slot-value-info (object-editor grid))))

(defmethod handle-action :after ((grid generic-grid) (action (eql :cancel)))
  (unless (typep grid 'generic-sub-grid)
    (defer-js "$(window).off('beforeunload');"))
  (clrhash (slot-value-info (object-editor grid))))

(defgeneric class-export-csv-header (class))
(defgeneric class-export-csv-row (object))

(defmethod class-export-csv-row (object)
  (let (full-line
        (leftover))
    (labels ((class-slots-length (class)
               (loop for slot in (all-slots class)
                     for db-type = (db-type slot)
                     if (eq (slot-definition-name slot) 'stamp-date)
                     do (progn)
                     else if (csv-header slot)
                     count t
                     else if (and (typep db-type '(cons (eql list) (cons symbol null)))
                                  (find-class (cadr db-type) nil))
                     sum (class-slots-length (find-class (cadr db-type)))
                     else if (label slot)
                     count t))
             (collect (object &key key-only sub-objects)
               (loop with class = (class-of object)
                     for slot in (all-slots class)
                     for db-type = (db-type slot)
                     for custom-printer = (csv-row slot)
                     for printer = (slot-printer slot)
                     for virtual = (virtual slot)
                     for value = (if virtual
                                     (funcall virtual object)
                                     (slot-value-using-class class object slot))
                     for skip = (and key-only
                                     (not (key slot)))
                     if (eq (slot-definition-name slot) 'stamp-date)
                     do (progn)
                     else if custom-printer
                     ;; Don't escape the custom printer
                     collect (and (not skip)
                                  (funcall custom-printer value))
                     else if (and (typep db-type '(cons (eql list) (cons symbol null)))
                                  (subtypep (cadr db-type) 'wfx-object))
                     append (process-list (if skip
                                              (if (car sub-objects)
                                                  (list (pop sub-objects))
                                                  (pop sub-objects))
                                              value)
                                          (second db-type))
                     else if printer collect
                     (with-output-to-string (str)
                       (when (and value
                                  (not skip))
                         (write-csv-field
                          (princ-to-string (funcall printer value))
                          str)))))
             (process-list (value type)
               (cond (value
                      (when (cdr value)
                        (push (cdr value) leftover))
                      (collect (car value)))
                     (t
                      (loop for i below (class-slots-length (find-class type))
                            collect "")))))
      (setf full-line
            (nconc (collect object)
                   (and (typep object 'wfx-versioned-object)
                        (list (format-universal-date-time (effective-date object))
                              (format-universal-date-time (stamp-date object))))))
      (if leftover
          (values (list* full-line
                         (loop while (find-if #'identity leftover)
                               collect (collect object :key-only t
                                         :sub-objects (loop for i below (length leftover)
                                                            collect (pop (elt leftover i))))))
                  t)
          full-line))))

(defmethod csv-exporter-header (type) nil)
(defmethod csv-exporter-row (type) nil)

(defmethod class-export-csv-header (prototype)
  (labels ((collect (class)
             (loop for slot in (all-slots class)
                   for db-type = (db-type slot)
                   if (eq (slot-definition-name slot) 'stamp-date)
                   do (progn)
                   else if (csv-header slot)
                   collect it
                   else if (and (typep db-type '(cons (eql list) (cons symbol null)))
                                (subtypep (cadr db-type) 'wfx-object))
                   append (collect (find-class (cadr db-type)))
                   else if (label slot)
                   collect it)))
      (nconc (collect (class-of prototype))
             (and (typep prototype 'wfx-versioned-object)
                  '("Effective Date"
                    "Stamp Date")))))

(defmethod export-csv ((grid generic-grid))
  (let* ((data (grid-filtered-rows grid))
         (class (row-object-class grid))
         (prototype (class-prototype class)))
    (when data
      (with-output-to-string (stream)
        (format stream "~a Export.~@[Filter: ~A,~]~@[ Search: ~s,~] Date: ~A ~%Context: ~A~2%"
                (label class)
                (grid-filter grid) (search-term grid)
                (current-date-time) (print-context))
        (flet ((format-row (list)
                 (loop for (field . rest) on list
                       do (princ (substitute #\Space #\Newline (princ-to-string field))
                                 stream)
                       when rest
                       do (write-char #\| stream))
                 (terpri stream)))
          (format-row (class-export-csv-header prototype))
          (loop for doc being the element of data
                do
                (multiple-value-bind (row multiple) (class-export-csv-row doc)
                  (if multiple
                      (loop for row in row
                            do (format-row (mapcar #'object-description row)))
                      (format-row (mapcar #'object-description row))))))))))
