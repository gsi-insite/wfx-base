(in-package :wfx-base)

(defvar *current-slot*)
(defvar *editor*)

(defun render-edit-field (name value
                          &key data-element
                            required
                            data-type
                            (type :text)
                            data (blank-allowed t)
                            min
                            max
                            posted-value
                            on-change
                            first-value
                            (cols 85)
                            (rows 5)
                            (mce t))
  (let ((value (or (and posted-value
                        (parameter name))
                   value)))
    (with-html
      (case type
        (:span
         (htm (:input :class "docs form-control"
                      :type "text"
                      :name name
                      :disabled t
                      :readonly t
                      :value (escape value))))
        (:select
         (render-select (or data-element name) data value
                        :blank-allowed blank-allowed
                        :on-change on-change
                        :first-value first-value))
        (:textarea
         (htm (:textarea :class (frmt "docs form-control~:[ no-mce~;~]~@[ ~a~]" mce (if required "required"))
                         :id name
                         :name name
                         :cols cols :rows rows
                         :onchange on-change
                         (str (escape value)))))
        (:password
         (htm (:input :type "password"
                      :class (frmt "form-control~@[ ~a~]" (if required "required"))
                      :name name
                      :value (escape value))))
        (:date
         (htm (:input :type "text"
                      :class (frmt "docs form-control date~@[ ~a~]" (if required "required"))
                      :name name
                      :id name
                      :value (escape value)
                      :onchange on-change)))
        (:checkbox
         (htm (:input :type "checkbox"
                      :class "docs"
                      :id name
                      :name name
                      :checked (if value "true")
                      :onchange on-change)))
        (t
         (htm
          (:input :type type
                  :class (frmt "docs form-control ~:[~;required~]~@[~a~]" required data-type)
                  :name name
                  :min min
                  :max max
                  :value (escape value))
          (:div :style "display:none;"
                :name (format nil "validate-~A" name)
                :id (format nil "validate-~A" name)
                (:img :src "/images/q-icon.png"))))))))



(defun render-select (name items value &key first-value blank-allowed
                                            allow-deselect
                                            on-change
                                            (element-name name))
  (let ((select (make-widget 'select
                             :name name
                             :element-name element-name
                             :css-class "docs form-control")))
    (setf (blank-allowed select) blank-allowed)
    (setf (first-item select) first-value)
    (setf (items select) items)
    (setf (allow-deselect select) allow-deselect)
    (setf (value select) value)
    (setf (on-change select) on-change)
    (render select)
    select))

(defun parse-money (string)
  (if (stringp string)
      (let* ((comma  (position #\, string))
             (string (if comma
                         (remove #\, string :start comma)
                         string))
             (dot (position #\. string))
             (start (position-if-not #'whitespace-p string))
             (last-non-whitespace (position-if-not #'whitespace-p string :from-end t))
             (end (and last-non-whitespace
                       (1+ last-non-whitespace))))
        (labels ((bad-number ()
                   (return-from parse-money))
                 (parse-integer-completely (start end)
                   (multiple-value-bind (number pos)
                       (parse-integer string :junk-allowed t :start start :end end)
                     (if (= end pos)
                         number
                         (bad-number))))
                 (parse-decimal (start end multiple)
                   (when (= start end)
                     (bad-number))
                   (let ((result 0))
                     (loop for i from start below end
                           for digit = (digit-char-p (char string i))
                           unless digit do (bad-number)
                           do
                           (setf multiple (* multiple 10)
                                 result (+ (* result 10) digit)))
                     (values result multiple))))
          (cond ((not start)
                 (bad-number))
                (dot
                 (let* ((whole (or (parse-integer-completely start dot)
                                   0)))
                   (multiple-value-bind (decimal multiple)
                       (parse-decimal (1+ dot) end
                                      (if (char= (char string start) #\-)
                                          -1
                                          1))
                     (cond ((zerop decimal)
                            whole)
                           ((zerop whole)
                            (/ decimal multiple))
                           (t
                            (/ (+ (* whole multiple) decimal)
                               multiple))))))
                (t
                 (parse-integer-completely start end)))))
      string))

(defun calc-percent (whole part)
  (if (and whole part
           (plusp whole))
      (* (/ part whole) 100)
      0))

(defvar *number-coma* nil)

(defun format-number (value)
  (typecase value
    (null "")
    (number
     (cond (*number-coma*
            (fmt-money value))
           ((integerp value)
            (format nil "~a" value))
           (t
            (format nil "~,2f" (float value)))))
    ((or string
         cons)
     value)
    (t
     (princ-to-string value))))

(defun period-date-to-universal (date-string)
  (if date-string
      (let ((split-date (split-string date-string #\-)))
        (encode-universal-time
         0 0 0
         (parse-integer (third split-date))
         (parse-integer (second split-date))
         (if (> (parse-integer (first split-date)) 1900)
             (parse-integer (first split-date))
             1901)
         *time-zone*))
      date-string))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun date-to-universal (date-string)
    (if date-string
        (if (stringp date-string)
            (let ((split-date (split-string date-string #\-)))
              (encode-universal-time
               0 0 0
               (parse-integer (third split-date))
               (parse-integer (second split-date))
               (if (> (parse-integer (first split-date)) 1900)
                   (parse-integer (first split-date))
                   1901)
               *time-zone*))
            date-string)
        date-string)))

(define-compiler-macro date-to-universal (&whole form date-string)
  (if (stringp date-string)
      (date-to-universal date-string)
      form))

(defun disable-controls (widget)
  (frmt
   "$(\"#~a :button, ~:*#~a select, ~:*#~a input\").attr(\"disabled\", \"disabled\")"
   (name widget)))

(defun disable-grid-buttons (widget)
  (let ((widget (if (typep widget 'grid)
                    (editor widget)
                    widget)))
   (and widget
        (frmt
         "$(\"#~a .btn-toolbar :button\").attr(\"disabled\", \"disabled\")"
         (name widget)))))

(defun bogus-result-p (x)
  (and (member x '(nil "false" :null "" "n/a"
                   "NIL")
               :test #'equalp)
       t))

(defun whitespace-p (char)
  (case char
    ((#\Space #\Newline #\Tab #\Return)
     t)))

(defun pretty-field-name (name)
  (let ((pretty
          (substitute #\Space #\-
                      (string-capitalize name))))
    (if (alexandria:ends-with-subseq " P" pretty)
        (let ((sub (subseq pretty 0 (- (length pretty) 1))))
          (setf (elt sub (- (length pretty) 2)) #\?)
          sub)
        pretty)))

(defparameter *tmp-directory* #p"~/hunchentoot-upload/")

(defun filter-pathname (string)
  (let ((string (nsubstitute-if #\-
                                (lambda (x)
                                  (member x '(#\Space #\/ #\\ #\Newline #\Tab #\Return)))
                                (nstring-downcase (copy-seq string)))))
    (if (need-trim string '(#\-))
        (string-trim '(#\-) string)
        string)))

(defun move-upload (file new-name &optional copy)
  (let ((new-path (merge-pathnames new-name *tmp-directory*)))
    (ensure-directories-exist new-path)
    (cond (copy
           (alexandria:copy-file file new-path)
           new-path)
          (t
           (nth-value 2 (rename-file file new-path))))))

(defun remove-non-id-chars (id)
  (ppcre:regex-replace-all "[^A-Za-z0-9-_:.]"
                           (substitute #\- #\Space id) ""))

(defgeneric object-description (object)
  (:documentation "Return a string descrbing the object."))

(defmethod object-description (object)
  (princ-to-string object))

(defmethod object-description ((object ratio))
  (format-money-for-export object))

(defmethod object-description ((object string))
  object)

(defmethod object-description ((object class))
  (make-name (class-name object)))

(defmethod object-description ((object symbol))
  (make-name object))

(defmethod object-description ((object cons))
  (with-output-to-string (str)
    (write-string "(" str)
    (loop for x = (pop object)
          do (write-string (object-description x) str)
             (cond ((null object)
                    (return))
                   ((atom object)
                    (write-string " . " str)
                    (write-string (object-description object) str)
                    (return)))
          (write-char #\Space str))
    (write-string ")" str)))

(defun custom-slot-p (slot)
  (typep slot 'customizable-effective-slot-definition))
