(defsystem wfx-base
  :depends-on (wfx
               xdb2
               dx-utils
               closer-mop
               bordeaux-threads
               lparallel)
  :serial t
  :components
  ((:file "packages")
   (:file "common")
   (:file "mop")
   (:file "db")
   (:file "allsorts")
   (:file "generic-slots")
   (:file "object-editor")
   (:file "generic-grid")
   (:file "entity")
   (:file "entity-relationships")
   (:file "users")
   (:file "copy")
   (:file "scheduler")
   (:file "map-editor")))
