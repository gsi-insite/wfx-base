(in-package :wfx-base)

(defstruct value-info
  value
  read-only
  data
  invalid
  dependants
  no-accept
  process-empty
  overriden-p
  list-type)

(defstruct invalid
  value
  reason)

(defvar *slot-value-info*)
(defvar *parent-slot-value-info*)
(declaim (hash-table *slot-value-info* *parent-slot-value-info*))

(defmethod ensure-value-info (slot-name)
  (alexandria:ensure-gethash slot-name *slot-value-info* (make-value-info)))

(defvar *sub-grids*)
(defvar *tabs*)
(defvar *form-id*)

(defun present-slot-body (slot name value info)
  (let ((db-type (db-type slot)))
    (with-html
      (cond ((custom-slot-p slot)
             (let ((parent
                     (value-info-value
                      (gethash 'parent *slot-value-info*))))
              (with-html
                (:div :class "parent-slot"
                      (present-value db-type
                                     nil
                                     (and parent
                                          (slot-value parent (slot-definition-name slot)))
                                     info))
                (:div :class "this-slot"
                      :style (and (not (value-info-overriden-p info))
                                  "display:none")
                      (present-value db-type
                                     name
                                     value info)))))
            (t
             (present-value db-type
                            name
                            value info))))))

(defun present-slot (editor slot object &key read-only)
  (let* ((*current-slot* slot)
         (class (class-of object))
         (slot-name (slot-definition-name slot))
         (name (string slot-name))
         (custom-slot (custom-slot-p slot)))
    (when custom-slot
      (slot-depend-on 'parent))
    (multiple-value-bind (info existing-info) (ensure-value-info slot-name)
      (let* ((virtual (virtual slot))
             (value (if existing-info
                        (value-info-value info)
                        (setf (value-info-read-only info) read-only
                              (value-info-overriden-p info) (and custom-slot
                                                                 (slot-overriden-p object slot))
                              (value-info-value info)
                              (if virtual
                                  (funcall virtual object)
                                  (slot-value-using-class class object slot)))))
             (invalid (shiftf (value-info-invalid info) nil)))
        (if read-only
            (with-html
              (:div :class "form-group"
                    (:label :class (format nil "~a control-label docs"
                                           (car (column-widths editor)))
                            (esc (label slot)))
                    (:div :class (cdr (column-widths editor))
                          (esc (funcall (slot-printer slot) value)))))
            (let* (result
                   (body (with-html-string
                           (setf result
                                 (present-slot-body slot name value info)))))
              (cond ((eq result :inline)
                     (with-html
                       (:div :class (and invalid "has-error")
                             (str body)
                             (when invalid
                               (htm
                                (:div :class "col-sm-offset-4"
                                      (:span :class "help-block"
                                             (esc invalid))))))))
                    ((eq result :tab)
                     (push (list (label slot)
                                 body)
                           *tabs*))
                    (t
                     (with-html
                       (:div :class (and invalid "has-error")
                             (:label :id (frmt "label-~a" (remove-non-id-chars name))
                                     :class (format nil "~a control-label docs"
                                                    (car (column-widths editor)))
                                     (esc (label slot)))
                             (:div :class (cdr (column-widths editor))
                                   :id (frmt "group-~a" (remove-non-id-chars name))
                                   (str (description slot))
                                   (str body)
                                   (when (and invalid
                                              (not (eq invalid t)))
                                     (htm
                                      (:span :class "help-block"
                                             (esc invalid)))))
                             (when custom-slot
                               (with-html
                                 (:div
                                  (:div :class "col-sm-1"
                                        (:div :class "checkbox"
                                              (:input :type "checkbox"
                                                      :class "override-slot"
                                                      :name (format nil "~a-override" name)
                                                      :checked (value-info-overriden-p info)))))))))))))))))


(defun accept-slot (slot)
  (let* ((*current-slot* slot)
         (slot-name (slot-definition-name slot))
         (name (string slot-name))
         (info (ensure-value-info slot-name))
         (db-type (db-type slot))
         (custom (custom-slot-p slot))
         (override (and custom
                        (equal (post-parameter (format nil "~a-override" name)) "true")))
         (posted (cond ((value-info-process-empty info))
                       ((value-info-list-type info)
                        (loop for (key . value) in (post-parameters*)
                              when (equal key name)
                              collect value))
                       (t
                        (post-parameter name)))))
    (cond ((and custom
                (not (setf (value-info-overriden-p info) override))))
          ((value-info-read-only info))
          ((value-info-no-accept info)
           ;; It was already processed by some other means
           (if (and (not (value-info-value info))
                    (required slot))
               (not (setf (value-info-invalid info) "Required field"))
               t))
          ((or (not (empty-p posted))
               (value-info-list-type info))
           (multiple-value-bind (value error) (accept-value db-type posted info)
             (setf (value-info-value info) value)
             (cond (error
                    (setf (value-info-invalid info) error)
                    nil)
                   ((and (not value)
                         (required slot))
                    (setf (value-info-invalid info) "Required field")
                    nil)
                   (t))))
          ((required slot)
           (setf (value-info-value info) nil)
           (setf (value-info-invalid info) "Required field")
           nil)
          (t))))

(defun info-slot-value (slot-name)
  (value-info-value (gethash slot-name *slot-value-info*)))

(defun parent-info-has-slot-p (slot-name)
  (gethash slot-name *parent-slot-value-info*))

(defun parent-info-slot-value (slot-name)
  (value-info-value (gethash slot-name *parent-slot-value-info*)))

(defun validate-slot (slot)
  (let* ((slot-name (slot-definition-name slot))
         (info (ensure-value-info slot-name))
         (*current-slot* slot))
    (if (or (value-info-invalid info)
            (not (value-info-value info)))
        t
        (not
         (setf (value-info-invalid info)
               (funcall (validate slot) (value-info-value info)))))))

(defun slot-depend-on (dependency)
  (let ((info (gethash dependency *slot-value-info*)))
    (if info
        (pushnew *current-slot* (value-info-dependants info) :test #'eq)
        (error "~s depends on ~s, but it's not found."
               (slot-definition-name *current-slot*)
               dependency))
    info))

(defun dependant-slot-value (slot-name)
  (let ((info (gethash slot-name *slot-value-info*)))
    (if info
        (value-info-value info)
        (error "~s depends on ~s, but it's not found."
               (slot-definition-name *current-slot*) slot-name))))

(defgeneric make-slot-printer (db-type)
  (:documentation
   "Return a function that can be used to print a value of DB-TYPE"))

(defmethod make-slot-printer (db-type)
  #'object-description)

(defmethod make-slot-printer ((db-type (eql 'string)))
  #'identity)

(defmethod make-slot-printer ((db-type cons))
  (make-slot-list-printer (car db-type) (cdr db-type)))

(defmethod make-slot-list-printer (type args)
  #'object-description)

(defmethod make-slot-list-printer ((type (eql 'list)) args)
  (let ((child-printer (make-slot-printer (car args))))
    (lambda (list)
      (if (consp list)
          (with-output-to-string (str)
            (loop for (x . next) on list
                  do (princ (funcall child-printer x) str)
                  when next
                  do (princ ", " str)))
          (object-description list)))))

(defgeneric present-value (type name value info))
(defgeneric accept-value (type value info))

(defmethod present-value ((type (eql 'string)) name value info)
  (with-html
    (:input :class "docs form-control"
            :name name
            :value (escape value))))

(defmethod accept-value ((type (eql 'string)) value info)
  (trim-whitespace value))

(defmethod present-value ((type (eql 'multi-line-string)) name value info)
  (render-edit-field
   name value
   :type :textarea))

(defmethod accept-value ((type (eql 'multi-line-string)) value info)
  (accept-value 'string value info))

(defmethod present-value ((type (eql 'script)) name value info)
  (render-edit-field
   name value
   :type :textarea
   :mce nil))

(defmethod accept-value ((type (eql 'script)) value info)
  (accept-value 'string value info))

(defmethod present-value ((type (eql 'integer)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'integer)) value info)
  (let ((string (trim-whitespace value)))
    (or (ensure-parse-integer string)
        (values value "Invalid integer"))))

(defmethod present-value ((type (eql 'percentage)) name value info)
  (present-value 'string name (format-money-for-export value) info))

(defmethod accept-value ((type (eql 'percentage)) value info)
  (let ((integer (parse-money value)))
    (cond ((not integer)
           (values value "Invalid integer"))
          ((<= 0 integer 100)
           integer)
          (t
           (values value "Percentage must be between 0 and 100.")))))

(defmethod present-value ((type (eql 'number)) name value info)
  (present-value 'string name (fmt-money value) info))

(defmethod make-slot-printer ((db-type (eql 'number)))
  #'fmt-money)

(defmethod accept-value ((type (eql 'number)) value info)
  (or (parse-money value)
      (values value "Invalid number")))

(defmethod present-value ((type (eql 'float)) name value info)
  (present-value 'string name (and value
                                   (format nil "~f" value)) info))

(defmethod make-slot-printer ((db-type (eql 'float)))
  (lambda (x)
    (and x
         (format nil "~f" x))))

(defmethod accept-value ((type (eql 'float)) value info)
  (or (parse-number value)
      (values value "Invalid number")))

(defmethod present-value ((type (eql 'longitude)) name value info)
  (present-value 'float name value info))

(defmethod present-value ((type (eql 'latitude)) name value info)
  (present-value 'float name value info))

(defmethod make-slot-printer ((db-type (eql 'longitude)))
  (make-slot-printer 'float))

(defmethod make-slot-printer ((db-type (eql 'latitude)))
  (make-slot-printer 'float))

(defmethod accept-value ((type (eql 'longitude)) value info)
  (let ((number (parse-number value)))
    (or (and (numberp number)
             (< -180 number 180)
             number)
        (values value "Invalid longitude"))))

(defmethod accept-value ((type (eql 'latitude)) value info)
  (let ((number (parse-number value)))
    (or (and (numberp number)
             (< -90 number 90)
             number)
        (values value "Invalid latitude"))))

(defmethod present-value ((type (eql 'entity)) name value info)
  (present-select (loop for entity across (entities)
                        when (find (xid entity) (context))
                        collect entity)
                  name value info :key #'entity-name))

(defmethod accept-value ((type (eql 'entity)) value info)
  (accept-select value info))

(defmethod present-value ((type (eql 'root-entity)) name value info)
  (let* ((roots (find-root-entities))
         (entities (loop for entity across (entities)
                         when (and (find (xid entity) (context))
                                   (find entity roots :key #'entity))
                         collect entity))
         (names (loop for i from 0
                      for entity in entities
                      collect (list i (entity-name entity)))))
    (setf (value-info-data info) entities)
    (render-select name names
                   (and (typep value 'entity)
                        (entity-name value))
                   :blank-allowed t)))

(defmethod accept-value ((type (eql 'root-entity)) value info)
  (accept-value 'entity value info))

(defmethod present-value ((type (eql 'email)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'email)) value info)
  (let ((value (trim-whitespace value)))
    (if (find #\@ value)
        value
        (values value "Invalid email address"))))

(defmethod present-value ((type (eql 'unique-email)) name value info)
  (present-value 'email name value info))

(defmethod accept-value ((type (eql 'unique-email)) value info)
  (multiple-value-bind (value invalid) (accept-value 'email value info)
    (cond (invalid
           (values value invalid))
          ((get-user value)
           (values value "User with this email already exists."))
          (value))))

(defmethod present-value ((type (eql 'password)) name value info)
  (with-html
    (:input :type "password"
            :class "docs form-control"
            :name name
            :value (escape value))))

(defmethod present-value ((type (eql 'confirm-password)) name value info)
  (present-value 'password name value info))

(defmethod accept-value ((type (eql 'password)) value info)
  value)

(defmethod accept-value ((type (eql 'confirm-password)) value info)
  (let ((password (dependant-slot-value 'password)))
    (if (and password
             (string/= value password))
        (values value "Passwords do not match")
        value)))

(defmethod present-value ((type (eql 'url)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'url)) value info)
  (accept-value 'string value info))

(defmethod make-slot-printer ((type (eql 'url)))
  #'identity
  ;; (lambda (x)
  ;;   (with-html (:a :href (frmt "/insite/~a" x) (esc x))))
  )

(defmethod present-value ((type (eql 'yes-no)) name value info)
  (render-select name '("Yes" "No") value :blank-allowed t))

(defmethod accept-value ((type (eql 'yes-no)) value info)
  (or (car (member value '("Yes" "No") :test #'equalp))
      (values nil "Bad value")))

(defmethod present-value ((type (eql 'date)) name value info)
  (render-edit-field name (format-universal-date value) :type :date))

(defmethod accept-value ((type (eql 'date)) value info)
  (or (parse-date value)
      (values value "Invalid date format")))

(defmethod make-slot-printer ((type (eql 'date)))
  #'format-date)

(defmethod present-value ((type (eql 'month)) name value info)
  (present-select *long-months* name value info))

(defmethod accept-value ((type (eql 'month)) value info)
  (accept-select value info))


(defmethod present-value ((type (eql 'boolean)) name value info)
  (with-html
    (:input :type "checkbox"
            :name name
            :id name
            :checked value)))

(defmethod accept-value ((type (eql 'boolean)) value info)
  (equal value "true"))

;;;
(defmethod present-value ((type (eql 'error-display)) name value info)
  (when value
    (setf (value-info-process-empty info) t)
    (with-html
      (:div :class "form-group"
            (:div :class "col-sm-offset-3 col-sm-7"
                  (:div :class "alert alert-info"
                        (esc value))))))
  :inline)

;;; Reset the error
(defmethod accept-value ((type (eql 'error-display)) value info))

;;;

(defun enumerate (sequence &key key)
  (loop for element being the element of sequence
        for i from 0
        collect (list i (cond ((not element)
                               "")
                              (key
                               (funcall key element))
                              (t
                               element)))))

(defun present-select (items name value info &key key)
  (let* ((pairs (and (not key)
                     (loop for item being the element of items
                           always (and (consp item)
                                       (= (length item) 2)))))
         (items (if (member-if (lambda (item)
                                 (or (not item)
                                     (equal "" item))) items
                                     :key #'item-value)
                    items
                    (list* (if pairs
                               '(nil "")
                               nil)
                           items))))
    (cond (pairs
           (setf (value-info-data info) (mapcar #'car items))
           (render-select name (enumerate items :key #'second)
                          (assoc-value value items)))
          (t
           (setf (value-info-data info) items)
           (render-select name (enumerate items :key key)
                          (if (and value key) (funcall key value) value))))))

(defun accept-select (value info)
  (let ((index (ensure-parse-integer value)))
    (if (and (typep index 'unsigned-byte)
             (< index (length (value-info-data info))))
        (elt (value-info-data info) index)
        (values nil "Bad value"))))

(defun present-allsort (sort name value info)
  (present-select (find-allsorts-for-select sort) name value info))

;;;

(defmethod present-value ((type cons) name value info)
  (present-list (car type) (cdr type) name value info))

(defmethod accept-value ((type cons) value info)
  (accept-list (car type) (cdr type) value info))

(defgeneric present-list (type parameters name value info))
(defgeneric accept-list (type parameters value info))

(defun eval-member-parameters (parameters)
  (if (consp parameters)
      parameters
      (eval parameters)))

(defmethod present-list ((type (eql 'member)) parameters name value info)
  (let* ((items (eval-member-parameters parameters))
         (items items))
    (present-select items name value info :key (if (every #'listp items)
                                                   nil
                                                   #'object-description))))

(defmethod accept-list ((type (eql 'member)) parameters value info)
  (accept-select value info))

(defmethod present-list ((type (eql 'multi-member)) parameters name value info)
  (setf (value-info-list-type info) t)
  (with-html
    (loop for x in (eval-member-parameters parameters)
          for desc = (object-description (item-description x))
          do (htm (:div :class "checkbox"
                        (:label (:input :type "checkbox"
                                        :name name
                                        :checked (member (item-value x) value :test #'equal))
                                (esc desc)))))))

(defmethod accept-list ((type (eql 'multi-member)) parameters value info)
  (loop for x in (eval-member-parameters parameters)
        for state in value
        when (equal state "true")
        collect (item-value x)))

(defmethod present-list ((type (eql 'integer)) parameters name value info)
  (present-value type name value info))

(defmethod accept-list ((type (eql 'integer)) parameters value info)
  (multiple-value-bind (value error) (accept-value type value info)
    (if error
        (values value error)
        (destructuring-bind (min &optional (max '*)) parameters
          (cond ((and (not (eq min '*))
                      (not (eq max '*)))
                 (if (<= min value max)
                     value
                     (values value (format nil "The value must be in the range of ~a-~a" min max))))
                ((not (eq max '*))
                 (if (<= value max)
                     value
                     (values value (format nil "The value must be less than or equal to ~a" max))))
                (t
                 (if (<= min value)
                     value
                     (values value (format nil "The value must be more than or equal to ~a" min)))))))))

(defun select-add (name type values)
  (let ((printer (make-slot-printer type)))
   (with-html
     (:div :class "select-add"
           (:input :type "text"
                   :class "form-control"
                   :id name)
           (:a :href (frmt "javascript:select_add_row(\"~a-table\",\"~:*~a\")"
                           name)
               :title "Add"
               (:span :class "grid-button glyphicon glyphicon-plus-sign"))
           (:table :id (frmt "~a-table" name)
                   (:thead (:tr (:th "") (:th "")))
                   (:tbody
                    (loop for value in values
                          for text = (if (invalid-p value)
                                         (invalid-value value)
                                         value)
                          do (htm (:tr
                                   (:td (:input :type "hidden" :name name :value (escape text))
                                        (esc
                                         (funcall printer text))
                                        (when (invalid-p value)
                                          (htm
                                           (:span :class "help-block"
                                                  (esc (invalid-reason value))))))
                                   (:td "")))))))
     (defer-js "(function(){~
var table = $('#~a-table').DataTable({info:false,searching:false, ordering: false,~
'language': {'emptyTable': 'Add new entries above'},~
paging:false,scrollY:'200px','scrollCollapse':true,~
'columnDefs': [{~
'targets': 1,'data': null,~
'defaultContent': '<a href=\"javascript:;\" title=\"Remove\"><span class=\"grid-button glyphicon glyphicon-remove-sign\"></a>'}]
});
$('#~:*~a-table tbody').on('click', 'a', function () {table.row($(this).parents('tr')).remove().draw(); })
})()"
       name))))

(defmethod present-list ((type (eql 'list)) parameters name value info)
  (let ((type (car parameters)))
    (cond ((subtypep type 'wfx-object)
           (setf (getf *sub-grids* *current-slot*) info)
           (throw 'sub-grid t))
          (t
           (setf (value-info-list-type info) t)
           (select-add name type (alexandria:ensure-list value))))))

(defmethod accept-list ((type (eql 'list)) parameters value info)
  (let ((type (car parameters)))
    (cond ((subtypep type 'wfx-object)
           (value-info-value info))
          (t
           (let (invalid)
             (values
              (loop for value in value
                    collect (multiple-value-bind (value reason)
                                (accept-value type value info)
                              (cond (reason
                                     (setf invalid t)
                                     (make-invalid :value value :reason reason))
                                    (t
                                     value))))
              invalid))))))

;;;

(defmethod present-list ((type (eql 'allsort)) parameters name value info)
  (present-allsort (car parameters) name value info))

(defmethod accept-list ((type (eql 'allsort)) parameters value info)
  (accept-select value info))

(defmethod present-list ((type (eql 'allsort-checkbox)) parameters name value info)
  (present-list 'multi-member (find-allsorts-for-validation (car parameters)) name value info))

(defmethod accept-list ((type (eql 'allsort-checkbox)) parameters  value info)
  (accept-list 'multi-member (find-allsorts-for-validation (car parameters)) value info))

;;;
