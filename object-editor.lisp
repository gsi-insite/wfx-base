(in-package :wfx-base)

(defclass object-editor (ajax-widget)
  ((object :initarg :object
           :initform nil
           :accessor object) 
   (slot-value-info :initarg :slot-value-info
                    :initform (make-hash-table :test #'eq)
                    :accessor slot-value-info)
   (parent :initarg :parent
           :initform nil
           :accessor parent)
   (info :initarg :info
         :initform nil
         :accessor info)
   (grid :initarg :grid
         :initform nil
         :accessor grid)
   (panel :initarg :panel
          :initform t
          :accessor panel)
   (columns :initarg :columns
            :initform 1
            :accessor columns)
   (column-widths :initarg :column-widths
                  :initform '("col-sm-4" . "col-sm-6")
                  :accessor column-widths))
  (:metaclass widget-class))

(defmethod object-editor-content (editor object))

(defmethod object-editor-action-js (editor type)
  (js-render-form-values (or (grid editor)
                             editor)
                         *form-id*
                         (js-pair "message-type" "action")
                         (js-pair "message-dest" (name editor))
                         (js-pair "action-type" type)))

(defmethod render-slots (editor object &key read-only-slots)
  (let (editors)
    (loop for slot in (all-slots (class-of object))
          when (db-type slot)
          do (catch 'sub-grid
               (push
                (with-html-string (present-slot editor slot object
                                                :read-only (or (find (slot-definition-name slot)
                                                                     read-only-slots)
                                                               (read-only slot))))
                editors)))
    (setf editors (nreverse editors))
    (loop while editors
          do (with-html
               (:div :class "form-group"
                     (loop for column below (columns editor)
                           do 
                           (str (or (pop editors)
                                    (return)))))))))

(defmethod object-editor-js (object form))

(defmethod object-editor-js ((object customized-object) form)
  (defer-js "$('#~a .parent-slot input, ~:* #~a .parent-slot select').attr('disabled', 'disabled')"
    form)
  (defer-js "$('#~a .override-slot').change(function(){$('.this-slot',$(this).closest('.form-group')).toggle()})"
    form))

(defmethod render ((editor object-editor) &key modal-edit
                                               read-only-slots
                                               (target editor))
  (let* ((object (object editor))
         (class (class-of object))
         (class-name (class-name class))
         (form-name (format nil "~a-form" class-name))
         (*editor* editor)
         (*form-id* form-name)
         (header (label class))
         (*slot-value-info* (slot-value-info editor))
         *sub-grids*
         *tabs*
         (main-content
           (with-html-string
             (render-slots editor object :read-only-slots read-only-slots)
             (object-editor-content editor object)
             (object-editor-js object form-name)))
         (tab-box
           (and (or *sub-grids*
                    *tabs*)
                (make-widget 'tab-box
                             :name (format nil "~a-tabs" (class-name class))
                             :header header
                             :icon "card--pencil")))
         (form (make-widget 'form :name form-name
                                  :header header
                                  :form-id form-name
                                  :panel (and (panel editor)
                                              (not tab-box)))))
    (unless modal-edit
      (with-html (:div :class "editing")))
    (assert (not (and *sub-grids* *tabs*)))
    (when tab-box
      (setf (buttons form) nil
            (tabs tab-box)
            (list (list header
                        (if *sub-grids*
                            (with-html-string
                              (render form
                                      :grid target
                                      :content main-content))
                            (with-html-string
                              (:div :class "form-horizontal"
                                    (str main-content))))))
            (body-content tab-box)
            (unless modal-edit
              (with-html-string
                (:div :class "btn-toolbar panel-footer"
                      (render-save-buttons target form-name)))))
      
      (when *sub-grids*
        (setf (tabs tab-box)
              (append (tabs tab-box)
                      (nreverse
                       (loop for (slot info) on *sub-grids* by #'cddr
                             for header = (label slot)
                             collect
                             (list header
                                   (let* ((class (second (db-type slot)))
                                          (grid
                                            (make-widget 'generic-sub-grid
                                                         :parent-grid (and (typep target 'grid)
                                                                           target)
                                                         :name (sub-name target (string class))
                                                         :title header
                                                         :row-object-class
                                                         (find-class class)))
                                          (read-only (getf (cddr (db-type slot)) :read-only)))
                                     (setf (info grid) info
                                           (initargs grid)
                                           (subst object :this (getf (cddr (db-type slot)) :initargs)))
                                     (if (eq read-only t)
                                         (setf (editable grid) nil)
                                         (setf (read-only-slots grid) read-only))
                                     (render-to-string grid))))))))
      (when *tabs*
        (setf (tabs tab-box)
              (append (tabs tab-box)
                      *tabs*))))
    (let ((content
            (with-html-string
              (cond (*tabs*
                     (htm
                      (:div :id form-name
                            (render tab-box))))
                    (tab-box
                     (render tab-box))
                    (t
                     (when modal-edit
                       (setf (buttons form) nil))
                     (render form
                             :grid target
                             :content main-content))))))
      (if modal-edit
          (let ((modal (make-widget 'modal
                                    :name (frmt "~a-modal" form-name)
                                    :form-id form-name
                                    :grid target)))
            (unless (eq modal-edit t)
              (setf (size modal) modal-edit))
            (setf (content modal) content)
            (render modal))
          (princ content)))

    (process-dependant-slots form-name *slot-value-info* editor)))

;;;

(defclass standalone-object-editor (object-editor)
  ((save-button-label :initarg :save-button-label
                      :initform "Save"
                      :accessor save-button-label)
   (target :initarg :target
                       :initform nil
                       :accessor target)
   (persist :initarg :persist
            :initform nil
            :accessor persist-on-save))
  (:metaclass widget-class))

(defmethod render-save-buttons ((editor standalone-object-editor) form-id &key)
  (let ((id (sub-name editor "save")))
    (with-html
     (:button :class "btn-primary btn"
              :id id
              :onclick
              (format nil "$(\"#~a\").attr(\"disabled\", true);~a"
                      id
                      (js-render-form-values (or (target editor)
                                                 editor)
                                             form-id
                                             (js-pair "message-type" "save")
                                             (js-pair "message-dest" (name editor))))
              (esc (save-button-label editor))))))

(defmethod handle-action ((editor standalone-object-editor) (action (eql :save)))
  (let* ((object (object editor))
         (*slot-value-info* (slot-value-info editor))
         (success (accept-doc object)))
    (when success
      (commit-values object *slot-value-info*)
      (when (persist-on-save editor)
        (persist object))
      t)))

(defgeneric object-editor-action (editor object action-type))

(defmethod handle-action ((editor object-editor) (action (eql :action)))
  (let* ((object (copy-object (object editor)))
         (*slot-value-info* (slot-value-info editor))
         (success (accept-doc object)))
    (when success
      (commit-values object *slot-value-info*)
      (object-editor-action editor object (post-parameter "action-type"))
      t)))

;;;
(defun update-dependent-slot (updated-info editor)
  (let ((object (object editor)))
    (with-output-to-string (str)
      (loop for slot in (reverse (value-info-dependants updated-info))
            for *current-slot* = slot
            for name = (slot-definition-name slot)
            for info = (gethash name *slot-value-info*)
            for id = (frmt "group-~a" (remove-non-id-chars (string name)))
            do
            (setf (value-info-value info) nil)
            (let (*widget-parameters*)
              (format str "$('#~a #~a').html('~a');~a"
                      *form-id* id
                      (replace-newlines
                       (escape-js
                        (with-html-string-no-indent
                          (catch 'sub-grid
                            (present-slot-body slot (string name) nil info)))))
                      (deferred-js)))
            (when (value-info-dependants info)
              (write-string (bind-depend *form-id* name editor)
                            str)
              (loop for dep in (value-info-dependants info)
                    do (write-string (update-dependent-slot info editor) str))))
      (let (*widget-parameters*)
        (object-editor-js object *form-id*)
        (format str (deferred-js))))))

(defun update-dependent-slots (editor name value values)
  (declare (ignore values))
  (let* ((*slot-value-info* (slot-value-info editor))
         (class (class-of (object editor)))
         (*form-id* (format nil "~a-form" (class-name class)))
         (*editor* editor)
         (*sub-grids*)
         (slot (find name (all-slots class)
                     :key #'slot-definition-name
                     :test #'string=))
         (info (gethash (slot-definition-name slot) *slot-value-info*)))
    (multiple-value-bind (value error) (accept-value (db-type slot) value info)
      (cond (error
             "null")
            (t
             (setf (value-info-value info) value)
             (update-dependent-slot info editor))))))

(defun bind-depend (form-id slot-name editor)
  (format nil
          "$('#~a #~a').change(function(){var value;~
 if($(this).is(':checkbox'))~
 value = $(this).is(':checked');
 else value = $(this).val();
 grid_depend(~s, ~s, '~a',value,[])});"
          form-id slot-name
          (script-name*) (name editor) slot-name))

(defun process-dependant-slots (form-id info editor)
  (loop for value being the hash-value of info
        using (hash-key slot-name)
        when (value-info-dependants value)
        do
        (defer-js (bind-depend form-id slot-name editor))))
;;;

(defgeneric commit-value (object type value)
  (:method (object type value)
    value))

(defun commit-values (object infos)
  (let ((class (class-of object)))
    (loop for slot in (class-slots class)
          when (db-type slot)
          do (let* ((name (slot-definition-name slot))
                    (info (gethash name infos))
                    (parent (custom-slot-p slot))
                    (override (value-info-overriden-p info)))
               (when info
                 (when parent
                   (override-slot object slot override))
                 (when (or (not parent)
                           override)
                   (setf (slot-value-using-class class object slot)
                         (commit-value object (db-type slot) (value-info-value info)))))))))

(defun accept-doc (doc)
  (let ((success t))
    (loop for slot in (all-slots (class-of doc))
          when (db-type slot)
          do (setf success
                   (and (accept-slot slot)
                        success)))
    (loop for slot in (class-slots (class-of doc))
          when (validate slot)
          do (setf success
                   (and (validate-slot slot)
                        success)))
    success))
