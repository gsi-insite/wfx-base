(in-package :wfx-base)

(defclass entity-relationship (entity-doc xid-doc)
  ((root :initarg :root
         :initform nil
         :accessor root
         :key t)
   (parent :initarg :parent
           :initform nil
           :accessor parent
           :key t)
   (children :initarg :children
             :initform ()
             :accessor children))
  (:collection "entity-relationships")
  (:metaclass wfx-versioned-class))

(defun entity-relationships-collection ()
  (get-collection (system-db) "entity-relationships"))

(defgeneric add-relationship-child (parent child))

(defun find-duplicate-entity-relationship (doc docs-list &key element)
  (dolist (dup docs-list)
    (let ((dup-entity (entity dup))
          (entity (entity doc)))
      (if (equal (get-val dup-entity element) (get-val entity element))
          (return-from find-duplicate-entity-relationship dup)))))

;;TODO: Should we not do a replace instead???
(defmethod add-relationship-child ((parent-relationship entity-relationship) child)
  ;;If child already exists dont-add duplicate
  (unless (find-duplicate-entity-relationship
           child (children parent-relationship)
           :element 'entity-name)
    (setf (get-val parent-relationship 'children)
          (append (children parent-relationship)
                  (if (listp child)
                      child
                      (list child))))))

(defun map-relationship-tree (relationship func)
  (labels ((rec (relationship)
               (funcall func relationship)
               (if (children relationship)
                   (dolist (entity-rel (children relationship))
                     (rec entity-rel)))))
      (rec relationship)))

(defun add-relationship-tree-child (root parent-eid child-relationship)
  (map-relationship-tree
   root
   (lambda (parent-relationship)
     (when (equal parent-eid (xid (entity parent-relationship)))
       (unless (find-duplicate-entity-relationship
                child-relationship (children parent-relationship) :element 'entity-name)
         (setf (root child-relationship) root)
         (setf (parent child-relationship) parent-relationship)
         (setf (children parent-relationship)
               (append (children parent-relationship) (list child-relationship)))
         (append (children parent-relationship) (list child-relationship)))))))

(defun get-relationship-tree-item-by-id (root relationship-id)
  (map-relationship-tree
   root
   (lambda (rel)
     (when (equal relationship-id (xid rel))
       (return-from get-relationship-tree-item-by-id
         rel)))))

(defun get-relationship-tree-item (root entity-id)
  (when root
    (map-relationship-tree
     root
     (lambda (rel)

       (when (equal entity-id (xid (entity rel)))
         (return-from get-relationship-tree-item
           rel))))))

(defun remove-relationship-tree-child (root child)
  (map-relationship-tree
   root
   (lambda (rel)
     (when (equal (xid (parent child)) (xid (entity rel)))
       (setf (children rel)
             (remove child (children rel)))))))

(defun get-entity-relationships-ordered (root-id entity-ids)
  (let ((rels ()))
    (map-relationship-tree
     (get-doc (entity-relationships-collection)
              root-id
              :element 'root)
     (lambda (rel)
       (when (find (xid (entity rel)) entity-ids)
         (setf rels (append rels (list rel))))))))


(defmethod match-entities ((doc entity-relationship) entity-ids)
  (find (xid (get-val doc 'entity)) entity-ids))

(defun get-root-entity (eid)
  (let ((root))
    (dolist (tree (coerce (entity-relationships) 'list))
      (map-relationship-tree
       tree
       (lambda (relationship)
         (when (equal eid (xid (entity relationship)))
          ; (break "?")
           (if (root relationship)
               (setf root (get-entity-by-id (root relationship)))
             (setf root (entity relationship)))))))
    root))

(defun get-root (eid)
  (find-doc
   (entity-relationships-collection)
   :test (lambda (doc)
           (equal eid (xid (entity doc))))))

(defun get-entity-branch (root entity-id)
  (let (result)
    (labels ((gerpx (root entity-id)
               (let* ((entity-rel (get-relationship-tree-item root entity-id))
                      (parent-rel (and entity-rel
                                       (parent entity-rel)
                                       (get-relationship-tree-item
                                        root
                                        (xid (parent entity-rel))))))
                 (when entity-rel
                   (push (entity entity-rel) result)
                   (when parent-rel
                     (gerpx root (xid (entity parent-rel))))))))
      (gerpx root entity-id))
    result))

(defun get-entity-branch-id-list (root entity-id)
  (mapcar #'xid (get-entity-branch root entity-id)))

(defun find-root-entities ()
  (loop with context = (context)
        for rel across (entity-relationships)
        if (member (xid (entity rel)) context)
        collect rel into context-roots
        else
        collect rel into rest-roots
        finally (return (nconc context-roots rest-roots))))

(defun find-entity-branches (entity)
  "Find entities higher up the tree."
  (loop for root in (find-root-entities)
        thereis (get-entity-branch root (xid entity))))

(defun entity-children (entity)
  (let ((rel
          (loop for root in (find-root-entities)
                thereis (get-relationship-tree-item root (xid entity))))
        result)
    (when rel
      (map-relationship-tree rel
                             (lambda (x)
                               (push (entity x) result))))
    result))

(defun tree-entities (root)
  (let ((result))
   (labels ((recurse (rel)
              (push (entity rel) result)
              (mapcar #'recurse (children rel))))
     (recurse root))
    result))

(defun entity-root-entity (entity)
  (get-root-entity (xid entity)))
