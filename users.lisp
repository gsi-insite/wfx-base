(in-package :wfx-base)

(defclass base-user (doc)
  ((email :initarg :email
          :initform nil
          :accessor email
          :db-type email
          :key t)
   (permissions :initarg :permissions
                :initform nil
                :accessor permissions
                :db-type permissions)
   (accessible-entities :initarg :accessible-entities
			:initform nil
			:accessor accessible-entities
                        :db-type entities)
   (super-user-p :initarg :super-user-p
                 :initform nil
                 :accessor super-user-p)
   (per-entity-permissions :initarg :per-entity-permissions
                           :initform nil
                           :accessor per-entity-permissions))
  (:metaclass wfx-versioned-class))

(defclass user (base-user)
  ((password :initarg :password
             :initform nil
             :accessor password)
   (salt :initarg :salt
         :accessor salt)
   (last-context :initarg :last-context
                 :initform nil
                 :accessor last-context)
   (ldap-entity :initarg :ldap-entity
                :initform nil
                :accessor ldap-entity)
   (ldap-name :initarg :ldap-name
              :initform nil
              :accessor ldap-name))
  (:collection "users")
  (:metaclass wfx-versioned-class))

(defun context ()
  (and (current-user)
       (multiple-value-bind (context found) (session-value 'context)
         (if found
             context
             (setf (context)
                   (remove-if-not #'get-entity-by-id
                                  (last-context (current-user))))))))

(defun (setf context) (entities)
  (when (current-user)
    (prog1
        (setf (session-value 'context) entities)
      (when (boundp '*current-page*)
        (setup-request-page *acceptor* *current-page*)))))

(defun users-collection ()
  (get-collection (system-db) "users"))

(defun get-user (email)
  (get-doc (users-collection) email :element 'email))

(defun find-users (criteria)
  (if criteria
      (find-docs 'vector
                 criteria
                 (users-collection))
      (users)))

;;;

(defun setup-permissions (user)
  (let ((hash (make-hash-table :test #'equal)))
    (cond ((super-user-p user)
           (loop with entities = (coerce (entities) 'list)
                 for (page . subs) in (permissions *acceptor*)
                 for perm = (alexandria:ensure-gethash page hash
                                                       (make-permission page
                                                                        (loop for sub in subs
                                                                              collect (cons sub entities))))
                 do
                 (setf (permission-entities perm) entities)))
          ((per-entity-permissions user)
           (loop for (entity . permissions) in (permissions user)
                 do
                 (loop for (page . sub) in permissions
                       for perm = (alexandria:ensure-gethash page hash (make-permission page))
                       do
                       (push entity (permission-entities perm))
                       (loop for sub in sub
                             do
                             (push entity (alexandria:assoc-value (permission-sub-permissions perm)
                                                                  sub :test #'equal))))))
          (t
           (loop for (page . sub) in (permissions user)
                 for perm = (make-permission page (mapcar #'alexandria:ensure-list sub))
                 do (setf (gethash page hash) perm))))
    hash))

(defun update-user-permissions ()
  (let* ((current *current-permissions*)
         (time (current-permissions-time current))
         (user (current-user)))
    (when (or (>= (effective-date user) time)
              (>= (modified (entities-collection)) time))
      (setf (current-permissions-permissions current) (setup-permissions user)
            (current-permissions-time current) (get-universal-time))
      (multiple-value-bind (context found) (session-value 'context)
        (when found
          (setf (session-value 'context)
                (remove-if-not #'get-entity-by-id context)))))))

(defun setup-page-permissions (page)
  (when *current-permissions*
    (update-user-permissions)
    (let ((perm (gethash (request-page-name page)
                         (current-permissions-permissions *current-permissions*))))
      (cond ((not perm)
             nil)
            ((permission-entities perm)
             (let ((entities (mapcar #'alexandria:ensure-list (permission-entities perm))))
               (loop for (sub . sub-entities) in (permission-sub-permissions perm)
                     do (loop for entity in sub-entities
                              do
                              (push sub (alexandria:assoc-value entities entity))))
               (loop for entry in entities
                     for (entity) = entry
                     when (member (xid entity) (context))
                     collect entry)))
            (t
             (let ((sub (mapcar #'car (permission-sub-permissions perm))))
               (loop for xid in (context)
                     for entity = (get-entity-by-id xid)
                     when entity
                     collect (cons entity sub))))))))
