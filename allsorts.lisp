(in-package :wfx-base)

(defclass allsort (doc)
  ((sort :initarg :sort
         :initform nil
         :accessor allsort-sort
         :key t)
   (sort-value :initarg :sort-value
               :initform nil
               :accessor sort-value
               :key t)
   (sort-order :initarg :sort-order
               :initform nil
               :accessor sort-order)
   (alternate-sort-order :initarg :alternate-sort-order
                         :initform nil
                         :accessor alternate-sort-order)
   (aa-sort-order :initarg :aa-sort-order
                  :initform nil
                  :accessor aa-sort-order)
   (description :initarg :description
                :initform nil
                :accessor description)
   (extended-description :initarg :extended-description
                         :initform nil
                         :accessor extended-description)
   (short-description :initarg :short-description
                      :initform nil
                      :accessor short-description))
  (:metaclass storable-versioned-class))

(defun allsorts-collection ()
  (get-collection (system-db) "allsorts"))

(defun allsorts ()
  (docs (allsorts-collection)))

(defmethod doc-collection ((doc allsort))
  (allsorts-collection))

(defun make-allsort (sort sort-value &key
                                       sort-order alternate-sort-order
                                       aa-sort-order description
                                       (extended-description "")
                                       short-description)
  (make-instance 'allsort
                 :sort sort
                 :sort-order sort-order
                 :sort-value sort-value
                 :alternate-sort-order alternate-sort-order
                 :aa-sort-order aa-sort-order
                 :description description
                 :extended-description extended-description
                 :short-description short-description
                 :top-level t))

(defun find-allsorts (sort &key (sort-key #'sort-order))
  (let ((docs (find-docs 'vector
                         (lambda (doc)
                           (string-equal (allsort-sort doc) sort))
                         (allsorts-collection))))
    (if sort-key
        (stable-sort (sort docs #'string< :key #'description)
                     (lambda (x y)
                       (cond
                         ((not (integerp y))
                          (integerp x))
                         ((not (integerp x))
                          nil)
                         (t (< x y))))
                     :key sort-key)
        docs)))

(defun find-allsorts-for-select (sort &key (sort-key #'sort-order))
  (map 'list (lambda (doc)
               (list (sort-value doc)
                     (description doc)))
       (find-allsorts sort :sort-key sort-key)))

(defun find-allsorts-for-validation (sort)
  (loop for allsort across (allsorts)
        when (equalp (allsort-sort allsort) sort)
        collect (sort-value allsort)))


(defun make-allsort-table ()
  (let ((ht (make-hash-table :test #'equal)))
    (loop for sort across (allsorts)
          do (setf (gethash (cons (allsort-sort sort)
                                  (sort-value sort))
                            ht)
                   sort))
    ht))

(defvar *allsort-cache*)

(defun add-allsort (sort sort-value
                    &rest args &key sort-order alternate-sort-order
                                    aa-sort-order (description sort-value)
                                    extended-description
                                    short-description)
  (declare (ignore alternate-sort-order
                   aa-sort-order
                   extended-description))
  (let* ((key (cons sort sort-value))
         (exists (gethash key *allsort-cache*)))
    (declare (dynamic-extent key))
    (cond ((not exists)
           (persist (apply #'make-allsort sort sort-value :description description args)))
          ((or (not (equal (description exists)
                           description))
               (not (equal (description exists)
                           description))
               (not (equal (sort-order exists)
                           sort-order))
               (not (equal (short-description exists)
                           short-description)))
           (apply #'reinitialize-instance exists :description description args)
           (persist exists)))))
